import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:hive/hive.dart';
import 'package:kt_dart/collection.dart';
import 'package:path_provider/path_provider.dart';
import 'package:todo/core/uitls/hive_box.dart';
import 'package:todo/features/data/repositories/todo_repository.dart';
import 'package:todo/features/domain/entities/todo.dart';

void main() {
  late TodoRepository repository;
  late HiveBox hiveBox;

  setUp(() async {
    final direcetory = await getApplicationDocumentsDirectory();
    Hive.init(direcetory.path);
    await Hive.openBox('todo');
    final Box box = Hive.box('todo');
    hiveBox = HiveBox(box);
    repository = TodoRepository(hiveBox);
    //uncomment the line below and run main if these tests fail, then comment it and run these tests again
    //hiveBox.box.deleteFromDisk();
  });

  const tTodo1 = Todo(id: '1', name: 'name', completed: true);
  const tTodo2 = Todo(id: '2', name: 'name', completed: false);
  const tTodoUpdate = Todo(id: '1', name: 'update', completed: true);
  test(
    "should add and delete database successfully",
    () async {
      //act
      final oAdd = repository.add(tTodo1);
      //assert
      expect(oAdd, const Right(unit));

      //act
      final oDelete = repository.delete(tTodo1);
      //assert
      expect(oDelete, const Right(unit));

      
      
    }
  );

  test(
    "should return KtList of Todo when get data succesfully",
    () async {
      //arrange
      repository.add(tTodo1);
      repository.add(tTodo2);
      //act
      final result = repository.getAll();
      //assert
      expect(result, Right(KtList.from([tTodo1, tTodo2])));
      repository.delete(tTodo1);
      repository.delete(tTodo2);
    }
  );

  test(
    "should return KtList of Todo completed when get data succesfully",
    () async {
      //arrange
      repository.add(tTodo1);
      repository.add(tTodo2);
      //act
      final result = repository.getCompleted();
      //assert
      expect(result, Right(KtList.from([tTodo1])));
      repository.delete(tTodo1);
      repository.delete(tTodo2);
    }
  );

  test(
    "should return KtList of Todo uncompleted when get data succesfully",
    () async {
      //arrange
      repository.add(tTodo1);
      repository.add(tTodo2);
      //act
      final result = repository.getUncompleted();
      //assert
      expect(result, Right(KtList.from([tTodo2])));
      repository.delete(tTodo1);
      repository.delete(tTodo2);
    }
  );

  test(
    "should update data succesfully",
    () async {
      //arrange
      repository.add(tTodo1);
      //act
      repository.update(tTodoUpdate);
      final result = repository.getCompleted();
      //assert
      expect(result, Right(KtList.from([tTodoUpdate])));
      repository.delete(tTodoUpdate);
    }
  );
}