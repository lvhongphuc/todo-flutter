import 'package:flutter_test/flutter_test.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart';
import 'package:todo/core/uitls/hive_box.dart';
import 'package:todo/features/data/repositories/todo_repository.dart';
import 'package:todo/features/domain/entities/todo.dart';
import 'package:todo/features/presentation/bloc/todo_action_bloc/todo_action_bloc.dart';

void main() async {
  late TodoActionBloc bloc;
  late TodoRepository repository;
  late HiveBox hiveBox;

  setUp(() async {
    final direcetory = await getApplicationDocumentsDirectory();
    Hive.init(direcetory.path);
    await Hive.openBox('todo');
    final Box box = Hive.box('todo');
    hiveBox = HiveBox(box);
    repository = TodoRepository(hiveBox);
    bloc = TodoActionBloc(repository);
    //uncomment the line below and run main if these tests fail, then comment it and run these tests again
    //hiveBox.box.deleteFromDisk();
  });

  const tStringEmpty = '';
  final tStringExceeded = 'a' * 101;
  const tStringMultiline = '\n';
  const tValid = 'test';

  test("initial state should be InitActionState", () async {
    //assert
    expect(bloc.state, const InitActionState());
  });

  group('input invalid', () {
    test("should emit Error with EMPTY_FAILURE_MESSAGE and InitActionState when the input empty", () async {
      //act
      bloc.add(const TodoActionSubmit(name: tStringEmpty, completed: true, idToUpdate: 'null'));
      //assert later
      const expected = [Error(message: EMPTY_FAILURE_MESSAGE), InitActionState()];
      expectLater(bloc.stream, emitsInOrder(expected));
    });

    test("should emit Error with EXCEEDED_FAILURE_MESSAGE and InitActionState when the input exceeded", () async {
      //act
      bloc.add(TodoActionSubmit(name: tStringExceeded, completed: true, idToUpdate: null));
      //assert later
      const expected = [Error(message: EXCEEDED_FAILURE_MESSAGE), InitActionState()];
      expectLater(bloc.stream, emitsInOrder(expected));
    });

    test("should emit Error with MULTILINE_FAILURE_MESSAGE and InitActionState when the input have multiline", () async {
      //act
      bloc.add(const TodoActionSubmit(name: tStringMultiline, completed: true, idToUpdate: null));
      //assert later
      const expected = [Error(message: MULTILINE_FAILURE_MESSAGE), InitActionState()];
      expectLater(bloc.stream, emitsInOrder(expected));
    });
  });

  group('input valid or delete event', () {
    test(
      "should emit Loading, Loaded and InitActionState with proper message when add a todo",
      () async {
        //act
        bloc.add(const TodoActionSubmit(name: tValid, completed: true, idToUpdate: null));
        //assert
        const expected = [Loading(), Loaded(message: CREATE_SUCCESS_MESSAGE), InitActionState()];
      expectLater(bloc.stream, emitsInOrder(expected));
      }
    );

    test(
      "should emit Loading, Loaded and InitActionState with proper message when update a todo",
      () async {
        //act
        bloc.add(const TodoActionSubmit(name: tValid, completed: true, idToUpdate: 'a'));
        //assert
        const expected = [Loading(), Loaded(message: UPDATE_SUCCESS_MESSAGE), InitActionState()];
      expectLater(bloc.stream, emitsInOrder(expected));
      }
    );

    test(
      "should emit Loading, Loaded and InitActionState with proper message when delete a todo",
      () async {
        //act
        const tTodo = Todo(id: '1', name: 'name', completed: true);
        repository.add(tTodo);
        bloc.add(const TodoActionDelete(todo: tTodo));
        //assert
        const expected = [Loaded(message: DELETE_SUCCESS_MESSAGE), InitActionState()];
        expectLater(bloc.stream, emitsInOrder(expected));
      }
    );
  });
}
