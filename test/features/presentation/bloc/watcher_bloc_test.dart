import 'package:flutter_test/flutter_test.dart';
import 'package:hive/hive.dart';
import 'package:kt_dart/collection.dart';
import 'package:path_provider/path_provider.dart';
import 'package:todo/core/uitls/hive_box.dart';
import 'package:todo/features/data/repositories/todo_repository.dart';
import 'package:todo/features/domain/entities/todo.dart';
import 'package:todo/features/presentation/bloc/watcher/watcher_bloc.dart';

void main() async {
  late WatcherBloc bloc;
  late TodoRepository repository;
  late HiveBox hiveBox;
  const tTodo1 = Todo(id: '1', name: 'name', completed: true);
  const tTodo2 = Todo(id: '2', name: 'name', completed: true);
  const tTodo3 = Todo(id: '3', name: 'name', completed: false);
  const tTodo4 = Todo(id: '4', name: 'name', completed: false);
  final rightCompletedValue = KtList.from([tTodo1, tTodo2]);
  final rightUncompletedValue = KtList.from([tTodo3, tTodo4]);
  setUp(() async {
    final direcetory = await getApplicationDocumentsDirectory();
    Hive.init(direcetory.path);
    await Hive.openBox('todo');
    final Box box = Hive.box('todo');
    hiveBox = HiveBox(box);
    repository = TodoRepository(hiveBox);
    bloc = WatcherBloc(repository);

    repository.add(tTodo1);
    repository.add(tTodo2);
    repository.add(tTodo3);
    repository.add(tTodo4);
    //uncomment the line below and run main if these tests fail, then comment it and run these tests again
    //hiveBox.box.deleteFromDisk();
  });

  test("should emit WatcherLoading, WatcherLoaded with completed todos", () async {
    //act
    bloc.add(const WatchCompleted());
    //assert
    final expected = [
      const WatcherLoading(),
      WatcherLoaded(todos: rightCompletedValue)
    ];
    expectLater(bloc.stream, emitsInOrder(expected));
  });

  test("should emit WatcherLoading, WatcherLoaded with completed todos", () async {
    //act
    bloc.add(const WatchUncompleted());
    //assert
    final expected = [
      const WatcherLoading(),
      WatcherLoaded(todos: rightUncompletedValue)
    ];
    expectLater(bloc.stream, emitsInOrder(expected));
  });
}
