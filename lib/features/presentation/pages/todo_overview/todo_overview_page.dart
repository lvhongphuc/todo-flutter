import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todo/features/domain/entities/todo.dart';
import 'package:todo/features/presentation/bloc/todo_action_bloc/todo_action_bloc.dart';
import 'package:todo/features/presentation/bloc/watcher/watcher_bloc.dart';
import 'package:todo/features/presentation/pages/todo/todo_page.dart';
import 'package:todo/main.dart';

class TodoOverviewPage extends StatefulWidget {
  const TodoOverviewPage({
    Key? key,
  }) : super(key: key);

  @override
  State<TodoOverviewPage> createState() => _TodoOverviewPageState();
}

class _TodoOverviewPageState extends State<TodoOverviewPage> {
  @override
  void initState() {
    super.initState();
    BlocProvider.of<WatcherBloc>(context).add(const WatchUncompleted());
  }

  int bottomIndex = 0;
  @override
  Widget build(BuildContext context) {
    return BlocListener<TodoActionBloc, TodoActionState>(
      listener: (context, state) {
        final String message = state is Error
            ? state.message
            : state is Loaded
                ? state.message
                : '';
        if (message.isNotEmpty) {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text(message),
            backgroundColor: state is Loaded ? Colors.green : Colors.red,
          ));
        }
      },
      child: BlocBuilder<WatcherBloc, WatcherState>(
        builder: (context, state) {
          return Scaffold(
            appBar: AppBar(
              title: const Text('Todo App'),
            ),
            floatingActionButton: FloatingActionButton(
              child: const Icon(Icons.add),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (_) => const TodoPage(todo: null),
                  ),
                );
              },
            ),
            body: state is WatcherLoaded
                ? ListView.builder(
                    itemCount: state.todos.size,
                    padding: const EdgeInsets.all(5),
                    itemBuilder: (context, index) {
                      final todo = state.todos[index];
                      return InkWell(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (_) => TodoPage(todo: todo),
                            ),
                          );
                        },
                        onLongPress: () => _showDeletionDialog(context, todo, bottomIndex),
                        child: Container(
                          margin: const EdgeInsets.all(10),
                          padding: const EdgeInsets.all(10),
                          decoration: BoxDecoration(
                              color:
                                  todo.completed ? Colors.green : Colors.red),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                todo.name,
                                style: const TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              IconButton(
                                onPressed: () =>
                                    _showDeletionDialog(context, todo, bottomIndex),
                                icon: const Icon(
                                  Icons.delete,
                                  color: Colors.white,
                                ),
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                  )
                : const Center(
                    child: CircularProgressIndicator(),
                  ),
            bottomNavigationBar: BottomNavigationBar(
              items: const [
                BottomNavigationBarItem(
                  label: 'Uncompleted',
                  icon: Icon(Icons.unpublished),
                ),
                BottomNavigationBarItem(
                  label: 'Completed',
                  icon: Icon(Icons.check_circle),
                ),
              ],
              currentIndex: bottomIndex,
              onTap: (val) {
                if (bottomIndex != val) {
                  setState(() {
                    bottomIndex = val;
                  });
                  BlocProvider.of<WatcherBloc>(context).add(
                    val == 0
                        ? const WatchUncompleted()
                        : const WatchCompleted(),
                  );
                }
              },
            ),
          );
        },
      ),
    );
  }

  void _showDeletionDialog(BuildContext context, Todo todo, int bottomIndex) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: const Text('Selected note:'),
          content: Text(
            todo.name,
            maxLines: 3,
            overflow: TextOverflow.ellipsis,
          ),
          actions: <Widget>[
            ElevatedButton(
              onPressed: () => Navigator.pop(context),
              child: const Text('CANCEL'),
              style: ElevatedButton.styleFrom(
                primary: Colors.black26
              ),
            ),
            ElevatedButton(
              onPressed: () {
                BlocProvider.of<TodoActionBloc>(context)
                    .add(TodoActionDelete(todo: todo));
                Navigator.pop(context);
                
                BlocProvider.of<WatcherBloc>(context).add(
                    bottomIndex == 0
                        ? const WatchUncompleted()
                        : const WatchCompleted(),
                );
              },
              child: const Text('DELETE'),
              style: ElevatedButton.styleFrom(
                primary: Colors.red
              ),

            ),
          ],
        );
      },
    );
  }
}