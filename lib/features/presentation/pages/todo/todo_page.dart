import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todo/features/domain/entities/todo.dart';
import 'package:todo/features/presentation/bloc/todo_action_bloc/todo_action_bloc.dart';
import 'package:todo/features/presentation/pages/todo_overview/todo_overview_page.dart';

class TodoPage extends StatefulWidget {
  final Todo? todo;
  const TodoPage({Key? key, required this.todo}) : super(key: key);

  @override
  State<TodoPage> createState() => _TodoPageState();
}

class _TodoPageState extends State<TodoPage> {
  final controller = TextEditingController();
  String inputStr = '';
  bool completed = false;
  @override
  void initState() {
    super.initState();
    if (widget.todo != null) {
      controller.text = widget.todo!.name;
      setState(() {
        completed = widget.todo!.completed;
        inputStr = widget.todo!.name;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<TodoActionBloc, TodoActionState>(
      listener: (BuildContext context, state) {
        if (state is Loaded) {
          Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(
                builder: (_) => const TodoOverviewPage(),
              ),
              (route) => false);
        }
      },
      builder: (context, state) {
        return Scaffold(
          appBar: AppBar(
            title: Text(widget.todo == null ? 'New todo' : 'Edit todo'),
            actions: [
              IconButton(
                onPressed: () => submit(),
                icon: const Icon(Icons.save),
              )
            ],
          ),
          body: Padding(
            padding: const EdgeInsets.all(20.0),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  TextField(
                    controller: controller,
                    onChanged: (val) {
                      inputStr = val;
                    },
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: 'Something to do',
                    ),
                  ),
                  const SizedBox(height: 20),
                  Row(
                    children: [
                      Checkbox(
                          value: completed,
                          onChanged: (val) {
                            setState(() {
                              completed = !completed;
                            });
                          }),
                      const Text('Completed'),
                    ],
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  void submit() {
    BlocProvider.of<TodoActionBloc>(context).add(TodoActionSubmit(
        name: inputStr, completed: completed, idToUpdate: widget.todo?.id));
    controller.clear();
    setState(() {
      inputStr = '';
    });
  }
}