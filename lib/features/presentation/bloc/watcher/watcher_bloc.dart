import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:kt_dart/collection.dart';
import 'package:todo/features/data/repositories/todo_repository.dart';
import 'package:todo/features/domain/entities/todo.dart';

part 'watcher_event.dart';
part 'watcher_state.dart';

class WatcherBloc extends Bloc<WatcherEvent, WatcherState> {
  final TodoRepository repository;
  WatcherBloc(this.repository) : super(WatcherInitial());

  @override
  Stream<WatcherState> mapEventToState(WatcherEvent event) async* {
    final todosOrFailure = event is WatchCompleted
        ? repository.getCompleted()
        : repository.getUncompleted();
    yield const WatcherLoading();
    yield* todosOrFailure.fold((_) async* {
      yield const WatcherError(message: 'Unexpected Error');
    }, (todos) async* {
      yield WatcherLoaded(todos: todos);
    });
  }
}
