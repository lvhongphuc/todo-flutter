part of 'watcher_bloc.dart';

abstract class WatcherState extends Equatable {
  const WatcherState();

  @override
  List<Object> get props => [];
}

class WatcherInitial extends WatcherState {}

class WatcherLoading extends WatcherState {
  const WatcherLoading();
}

class WatcherLoaded extends WatcherState {
  final KtList<Todo> todos;

  const WatcherLoaded({required this.todos});

  @override
  List<Object> get props => [todos];
}

class WatcherError extends WatcherState {
  final String message;

  const WatcherError({required this.message});

  @override
  List<Object> get props => [message];
}
