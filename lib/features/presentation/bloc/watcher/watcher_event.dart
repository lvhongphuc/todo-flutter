part of 'watcher_bloc.dart';

abstract class WatcherEvent extends Equatable {
  const WatcherEvent();

  @override
  List<Object> get props => [];
}

class WatchCompleted extends WatcherEvent {
  const WatchCompleted();
}

class WatchUncompleted extends WatcherEvent {
  const WatchUncompleted();
}