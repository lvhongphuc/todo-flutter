part of 'todo_action_bloc.dart';

abstract class TodoActionState extends Equatable {
  const TodoActionState();

  @override
  List<Object> get props => [];
}

class InitActionState extends TodoActionState {
  const InitActionState();
}

class Loading extends TodoActionState {
  const Loading();
}

class Loaded extends TodoActionState {
  final String message;
  const Loaded({required this.message});
  @override
  List<Object> get props => [message];
}

class Error extends TodoActionState {
  final String message;
  const Error({required this.message});
  
  @override
  List<Object> get props => [message];
}
