part of 'todo_action_bloc.dart';

abstract class TodoActionEvent extends Equatable {
  const TodoActionEvent();
  @override
  List<Object> get props => [];
}

class TodoActionSubmit extends TodoActionEvent {
  final String name;
  final bool completed;
  final String? idToUpdate;

  const TodoActionSubmit({
    required this.name,
    required this.completed,
    required this.idToUpdate,
  });
  @override
  List<Object> get props => [name, completed, idToUpdate!];
}

class TodoActionDelete extends TodoActionEvent {
  final Todo todo;

  const TodoActionDelete({required this.todo});
}
