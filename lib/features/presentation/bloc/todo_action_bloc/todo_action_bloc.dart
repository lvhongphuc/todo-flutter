// ignore_for_file: constant_identifier_names

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:todo/core/failures.dart';
import 'package:todo/core/uitls/input_validation.dart';
import 'package:todo/features/data/repositories/todo_repository.dart';
import 'package:todo/features/domain/entities/todo.dart';
import 'package:uuid/uuid.dart';

part 'todo_action_event.dart';
part 'todo_action_state.dart';

const EMPTY_FAILURE_MESSAGE = 'Todo name must not be empty';
const EXCEEDED_FAILURE_MESSAGE = 'Todo name must not be over 60 characters';
const MULTILINE_FAILURE_MESSAGE = 'Todo name must be in 1 line';
const UNEXPECTED_FAILURE_MESSAGE = 'Unexpected error';
const CREATE_SUCCESS_MESSAGE = 'Create successful';
const UPDATE_SUCCESS_MESSAGE = 'Update successful';
const DELETE_SUCCESS_MESSAGE = 'Delete successful';

class TodoActionBloc extends Bloc<TodoActionEvent, TodoActionState> {
  final TodoRepository repository;

  TodoActionBloc(this.repository) : super(const InitActionState());

  @override
  Stream<TodoActionState> mapEventToState(TodoActionEvent event) async* {
    if (event is TodoActionSubmit) {
      // fold() help us retracts Either type
      yield* inputValidation(event.name).fold(
        (failure) async* {
          yield Error(message: _mapInputFailureToMessage(failure));
          yield const InitActionState();
        },
        (name) async* {
          yield const Loading();
          final failureOrSuccess = event.idToUpdate == null
              ? repository.add(Todo(
                  id: const Uuid().v1(),
                  name: name,
                  completed: event.completed,
                ))
              : repository.update(Todo(
                  id: event.idToUpdate!,
                  name: name,
                  completed: event.completed,
                ));
          yield* failureOrSuccess.fold((failure) async* {
            yield const Error(message: UNEXPECTED_FAILURE_MESSAGE);
            yield const InitActionState();
          }, (_) async* {
            yield Loaded(message: event.idToUpdate == null ? CREATE_SUCCESS_MESSAGE : UPDATE_SUCCESS_MESSAGE);
            yield const InitActionState();
          });
        },
      );
    } else if (event is TodoActionDelete) {
      yield* repository.delete(event.todo).fold((l) async* {
        yield const Error(message: UNEXPECTED_FAILURE_MESSAGE);
        yield const InitActionState();
      }, (r) async* {
        yield const Loaded(message: DELETE_SUCCESS_MESSAGE);
        yield const InitActionState();
      });
    }
  }

  String _mapInputFailureToMessage(InputFailure failure) {
    switch (failure.runtimeType) {
      case EmptyInputFailure:
        return EMPTY_FAILURE_MESSAGE;
      case ExceededLengthInputFailure:
        return EXCEEDED_FAILURE_MESSAGE;
      case MultilineInputFailure:
        return MULTILINE_FAILURE_MESSAGE;
      default:
        return 'Unexpected Error';
    }
  }
}
