import 'package:equatable/equatable.dart';

class Todo extends Equatable {
  final String id;
  final String name;
  final bool completed;

  const Todo({
    required this.id,
    required this.name,
    required this.completed,
  });

  @override
  List<Object?> get props => [id, name, completed];
}
