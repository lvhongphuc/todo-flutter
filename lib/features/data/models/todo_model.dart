import 'package:todo/features/domain/entities/todo.dart';
class TodoModel {
  String? id;
  final String name;
  final bool completed;

  TodoModel({
    this.id,
    required this.name,
    required this.completed,
  });

  factory TodoModel.fromJson(String id, Map<String, dynamic> json) {
    return TodoModel(
      id: id,
      name: json['name'],
      completed: json['completed'] == 'true',
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'completed': completed.toString(),
    };
  }

  factory TodoModel.fromDomain(Todo todo) {
    return TodoModel(
      id: todo.id,
      name: todo.name,
      completed: todo.completed,
    );
  }

  Todo toDomain() {
    return Todo(
      id: id!,
      name: name,
      completed: completed,
    );
  }
}
