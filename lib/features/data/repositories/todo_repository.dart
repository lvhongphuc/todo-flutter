import 'package:hive/hive.dart';
import 'package:kt_dart/kt.dart';
import 'package:todo/core/uitls/hive_box.dart';
import 'package:todo/features/data/models/todo_model.dart';
import 'package:todo/features/domain/entities/todo.dart';
import 'package:todo/core/failures.dart';
import 'package:dartz/dartz.dart';
class TodoRepository {
  final HiveBox hiveBox;
  final Box<dynamic> box;
  TodoRepository(this.hiveBox) : box = hiveBox.box;

  /// get all todos return Right(list of todo) if it successes, Left(Failure) if fails
  Either<UnexpectedFailure, KtList<Todo>> getAll() {
    try {
      return Right(KtList.from(box.keys.map((key) =>
          TodoModel.fromJson(key, Map<String, dynamic>.from(box.get(key)))
              .toDomain())));
    } catch (_) {
      return const Left(UnexpectedFailure());
    }
  }

  Either<UnexpectedFailure, KtList<Todo>> _getCompletedOrUncompleted(bool completed) {
    return getAll().fold(
      (failure) => Left(failure),
      (todos) =>
          Right(todos.filter((todo) => todo.completed == completed).toList()),
    );
  }

  /// get completed todo return Right(list of todo) if it successes, Left(Failure) if fails
  Either<UnexpectedFailure, KtList<Todo>> getCompleted() {
    return _getCompletedOrUncompleted(true);
  }

  Either<UnexpectedFailure, KtList<Todo>> getUncompleted() {
    return _getCompletedOrUncompleted(false);
  }

  /// return Right(Unit) (like nothing) if successes, Left(Failure) if fails 
  Either<UnexpectedFailure, Unit> add(Todo todo) {
    try {
      TodoModel model = TodoModel.fromDomain(todo);
      box.put(model.id, model.toJson());
      return const Right(unit);
    } catch (_) {
      return const Left(UnexpectedFailure());
    }
  }

  /// return Right(Unit) (like nothing) if successes, Left(Failure) if fails 
  Either<UnexpectedFailure, Unit> delete(Todo todo) {
    try {
      box.delete(TodoModel.fromDomain(todo).id);
      return const Right(unit);
    } catch (_) {
      return const Left(UnexpectedFailure());
    }
  }

  /// return Right(Unit) (like nothing) if successes, Left(Failure) if fails 
  Either<UnexpectedFailure, Unit> update(Todo todo) {
    try {
      delete(todo);
      add(todo);
      return const Right(unit);
    } catch (_) {
      return const Left(UnexpectedFailure());
    }
  }
}
