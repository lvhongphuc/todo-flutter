import 'package:equatable/equatable.dart';

class UnexpectedFailure extends Equatable {
  const UnexpectedFailure();
  @override
  List<Object> get props => [];
}

abstract class InputFailure extends Equatable {
 const InputFailure();
  @override
  List<Object> get props => [];
}

class EmptyInputFailure extends InputFailure {}

class ExceededLengthInputFailure extends InputFailure {}

class MultilineInputFailure extends InputFailure {}