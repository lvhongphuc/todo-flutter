import 'package:dartz/dartz.dart';
import 'package:todo/core/exceptions.dart';
import 'package:todo/core/failures.dart';

Either<InputFailure, String> inputValidation(String str) {
  try {
    if (str.isEmpty) {
      throw EmptyInputException();
    }
    else if (str.length > 100) {
      throw ExceededLengthInputException();
    }
    else if (str.contains('\n')) {
      throw MultilineInputException();
    }
    else {
      return Right(str);
    }
  } on EmptyInputException {
    return Left(EmptyInputFailure());
  } on ExceededLengthInputException {
    return Left(ExceededLengthInputFailure());
  } on MultilineInputException {
    return Left(MultilineInputFailure());
  }
}