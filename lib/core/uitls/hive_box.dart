import 'package:hive/hive.dart';

class HiveBox {
  final Box box;

  HiveBox(this.box);
}