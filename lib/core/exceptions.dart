class EmptyInputException implements Exception {}

class ExceededLengthInputException implements Exception {}

class MultilineInputException implements Exception {}