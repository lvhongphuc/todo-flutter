import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart';
import 'package:todo/features/domain/entities/todo.dart';
import 'package:todo/features/presentation/bloc/todo_action_bloc/todo_action_bloc.dart';
import 'package:todo/features/presentation/bloc/watcher/watcher_bloc.dart';
import 'package:todo/features/presentation/pages/todo_overview/todo_overview_page.dart';
import 'package:todo/injection.dart';
import 'injection.dart' as di;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final direcetory = await getApplicationDocumentsDirectory();
  Hive.init(direcetory.path);
  // Save data with Hive
  await Hive.openBox('todo');
  // Service locator
  await di.init();
  runApp(const App());
}

class App extends StatefulWidget {
  const App({Key? key}) : super(key: key);

  @override
  State<App> createState() => _AppState();
}

class _AppState extends State<App> {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => sl<WatcherBloc>(),
        ),
        BlocProvider(
          create: (context) => sl<TodoActionBloc>(),
        ),
      ],
      child: const MaterialApp(
        home: TodoOverviewPage(),
        debugShowCheckedModeBanner: false,
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    Hive.close();
  }
}




