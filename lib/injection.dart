import 'package:get_it/get_it.dart';
import 'package:hive/hive.dart';
import 'package:todo/core/uitls/hive_box.dart';
import 'package:todo/features/data/repositories/todo_repository.dart';
import 'package:todo/features/presentation/bloc/todo_action_bloc/todo_action_bloc.dart';
import 'package:todo/features/presentation/bloc/watcher/watcher_bloc.dart';

final sl = GetIt.instance;

Future<void> init() async {
  //Bloc
  sl.registerFactory(() => WatcherBloc(sl()));
  sl.registerFactory(() => TodoActionBloc(sl()));

  //Repository
  sl.registerLazySingleton(() => TodoRepository(sl()));

  //Data
  final Box box = Hive.box('todo');
  sl.registerLazySingleton(() => HiveBox(box));
}